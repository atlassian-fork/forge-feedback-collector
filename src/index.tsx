import ForgeUI, {
    Button,
    ConfigForm,
    Form,
    Fragment,
    Macro,
    Option,
    Select,
    Text,
    TextArea,
    TextField,
    render,
    useAction,
    useConfig,
    useProductContext,
    useState
} from "@forge/ui";
import api from "@forge/api";

import { createIssueRequestBody } from "./issue";

type FormData = {
    summary: string;
    description: string;
};

enum SubmissionState {
    Success,
    Error,
    NotSubmitted
}

const fetchProjects = async () => {
    const response = await api.asApp().requestJira("/rest/api/3/project?expand=issueTypes", {
        headers: {
            "Content-Type": "application/json"
        },
        method: "GET"
    });

    const responseBody = await response.json();
    return responseBody;
};

const Config = () => {
    const [projects] = useAction(value => value, async () => await fetchProjects());

    // Ideally, we could make the list of issue types shown dependent on the project selected
    // However, this is a current limitation of Forge UI. There may be other workarounds to this problem, like
    // requiring the user to open the config form twice. But for now, we need to hardcode the list of issue types.
    const issueTypes = ["Improvement", "Task", "Bug", "New Feature"];

    return (
        <ConfigForm>
            <Select label="Project" name="projectKey">
                {projects && projects.map(project => {
                    const prettyName = `${project.name} (${project.key})`;
                    return (
                        <Option label={prettyName} value={project.key} />
                    )
                })}
            </Select>
            <Select label="Project" name="issueTypeName">
                {
                    issueTypes.map(issueType => {
                        return (
                            <Option label={issueType} value={issueType}/>
                        )
                    })
                }
            </Select>
        </ConfigForm>
    )
};

const App = () => {
    const { accountId } = useProductContext();
    const config = useConfig();
    const [state, setState] = useState<SubmissionState>(SubmissionState.NotSubmitted);
    const [issueCreated, setIssueCreated] = useState(null);

    const handleSubmit = async (formData: FormData) => {
        const { summary, description } = formData;

        const response = await api.asApp().requestJira("/rest/api/3/issue", {
            headers: {
                "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(
                createIssueRequestBody({
                    projectKey: config && config.projectKey,
                    reporterAccountId: accountId,
                    issueTypeName: config && config.issueTypeName,
                    summary,
                    description
                })
            )
        });

        const responseBody = await response.json();

        setState(response.ok ? SubmissionState.Success : SubmissionState.Error);
        setIssueCreated(response.ok ? responseBody : null);
    };

    if (!config) {
        return (
            <Text content="This app requires configuration before use."/>
        )
    }

    if (state === SubmissionState.NotSubmitted) {
        return (
            <Form onSubmit={handleSubmit}>
                <Text content="Submit feedback" />
                <TextField label="Summary" name="summary" />
                <TextArea label="Submit feedback" name="description" />
            </Form>
        );
    }

    return (
        <Fragment>
            <Text
                content={
                    issueCreated !== null
                        ? `Feedback submitted successfully! Your ticket is [${issueCreated.key}](/browse/${issueCreated.key})`
                        : "Something went wrong."
                }
            />
            <Button
                text={state === SubmissionState.Success ? "Create more feedback" : "Try again"}
                onClick={() => setState(SubmissionState.NotSubmitted)}
            />
        </Fragment>
    );
};

export const run = render(<Macro app={<App />} config={<Config />}/>);

export interface Issue {
  projectKey: string;
  issueTypeName: string;
  reporterAccountId: string;
  summary: string;
  description: string;
}

export const createAdf = (content: string) => ({
  type: "doc",
  version: 1,
  content: [
    {
      type: "paragraph",
      content: [{ text: content, type: "text" }]
    }
  ]
});

export const createIssueRequestBody = (issue: Issue) => ({
  fields: {
    project: {
      key: issue.projectKey
    },
    issuetype: {
      name: issue.issueTypeName
    },
    reporter: {
      id: issue.reporterAccountId
    },
    summary: issue.summary,
    description: createAdf(issue.description)
  }
});
